(ns build-utils
  (:require [boot.util :as util]
            [boot.core :as core]))

(defn read-dependencies! []
  (let [deps (read-string (slurp "resources/dependencies.edn"))]
    deps))

(defn- generate-lein-project-file!
  "Generates leiningen project file."
  [& {:keys [keep-project] :or {:keep-project true}}]

  (require 'clojure.java.io)
  (let [pfile ((resolve 'clojure.java.io/file) "project.clj")
        ; Only works when pom options are set using task-options!
        {:keys [project version]} (:task-options (meta #'boot.task.built-in/pom))
        prop #(when-let [x (core/get-env %2)] [%1 x])
        head (list* 'defproject (or project 'boot-project) (or version "0.0.0-SNAPSHOT")
               (concat
                 (prop :url :url)
                 (prop :license :license)
                 (prop :description :description)
                 [:dependencies (core/get-env :dependencies)
                  :source-paths (vec (concat (core/get-env :source-paths)
                                             (core/get-env :resource-paths)))
                  :repositories (core/get-env :repositories)]))
        proj (util/pp-str head)]
      (if-not keep-project (.deleteOnExit pfile))
      (spit pfile proj)))

(defn- modified-files? [before-fileset after-fileset regexes]
  (->> (core/fileset-diff before-fileset after-fileset)
       core/input-files
       (core/by-re regexes)
       not-empty))

(core/deftask lein-generate
  "Generate a leiningen `project.clj` file.
   This task generates a leiningen `project.clj` file based on the boot
   environment configuration, including project name and version (generated
   if not present), dependencies, and source paths. Additional keys may be added
   to the generated `project.clj` file by specifying a `:lein` key in the boot
   environment whose value is a map of keys-value pairs to add to `project.clj`."
  []

  (let [fs-prev-state (atom nil)]
    (core/with-pre-wrap fileset
      (when (or (not @fs-prev-state)
                (modified-files? @fs-prev-state fileset #{#"dependencies\.edn"}))
        (util/info "Regenerating project clj...\n")
        (generate-lein-project-file! :keep-project true)
        (reset! fs-prev-state fileset))
      fileset)))

(core/deftask update-deps
  ""
  []

  (let [fs-prev-state (atom nil)]
    (core/with-pre-wrap fileset
      (when (or (not @fs-prev-state)
                (modified-files? @fs-prev-state fileset #{#"dependencies\.edn"}))
        (util/info "Dependencies changed, updating...\n")
        (core/set-env! :dependencies (fn [deps] (read-dependencies!)))
        (reset! fs-prev-state fileset))
      fileset)))
