(ns thesis.frontend.async
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [cljs.core.async :as async :refer [<! >!]]
            [cljs.core.match :refer-macros [match]]
            [taoensso.timbre :as log]
            [taoensso.sente.packers.transit :as sente-transit]
            [thesis.common.utils.transit :as transit-utils]
            [taoensso.sente :as sente]
            [cljs-uuid-utils.core :as uuid]
            [cljs.core.async.impl.protocols :as async-protocols]))

(declare connect!)
(declare disconnect!)

(defonce client-id
  ; "Client id."
  (atom nil))

(defonce connected?
  ; "Current connection state for client."
  (atom false))

(defonce ^:private send-queue
  ; "Queue of messages to be sent server-side."
  (async/chan))

(defonce ^:private state-update-channel
  ; "Queue to notify the send loop of connection state change."
  (async/chan))

(defonce ^:private sente-async
  ; "Holds current sente state."
  (atom nil))

(defn update-client-id!
  "Updates current client id (forces reconnection)."
  [new-client-id]

  (disconnect!)
  (reset! client-id new-client-id)
  (connect!))

(defmulti async-handler
  "Handles an asynchronous message received from the server."
  (fn [event payload & metadata] event))

(defmethod async-handler :default ; Fallback
  [event payload metadata]

  (log/debug "Unhandled event:" event payload metadata))

(defn- get-async-url
  "Return url the async client should connect to."
  [protocol host path type]

  (log/debug "protocol=" protocol "host=" host "path=" path "type=" type)
  (str "ws://localhost:3000" path))

(defn- run-send-loop! [sente-send-fn]
  (let [run-pump? (async/chan)
        stop-fn (fn []
                  (async/close! run-pump?))]

    (go-loop []
      (when-not (async-protocols/closed? run-pump?)
        (when (not @connected?)
          (log/debug "Async not connected, WAITING ON CONNECTION!")
          (<! state-update-channel)
          (recur))

        (let [[result ch] (async/alts! [send-queue run-pump?])]
          (when-let [{:keys [event try]} result]
            (log/debug "Sending event: " event)
            (sente-send-fn event)
            (recur))))

      (log/debug "Async disconnected."))

    stop-fn))

(defn- handle-receive [{:keys [event send-fn] :as ev-msg}]
  (let [[id data :as ev] event]
    (log/debug "Event:" event)
    (log/debug "Event id:" id )
    (match event
      [:chsk/handshake  _]
      (log/debug "Got handshake!")

      ; first-open == not a reconnect
      [:chsk/state ({:first-open? true} :as state)]
      (do
        (log/debug "Channel socket successfully established!")
        (reset! connected? true)
        (async/put! state-update-channel state))

      [:chsk/state ({:open true} :as state)]
      (do
        (log/debug "Channel socket re-connected!")
        (reset! connected? true)
        (async/put! state-update-channel state))

      [:chsk/state ({:open false} :as state)]
      (do
        (log/debug "Channel socket closed!")
        (reset! connected? false)
        (async/put! state-update-channel state))

      [:chsk/state state]
      (do
        (log/debug "Chsk state change:" state)
        (async/put! state-update-channel state))

      [:chsk/recv _]
      (do
        (log/debug "Push event from server")
        (let [{:keys [?data]} ev-msg]
          (log/debug "Event:" ?data)
          (let [[event {:keys [payload reply-to]}] ?data
                ;event-info {:payload payload}
                metadata (when reply-to {:reply-to reply-to})]
            (log/debug "Dispatching event:" event payload metadata)
            (async-handler event payload metadata))))

      _
      (log/debug "Unmatched event:" ev))))

(defn make-client-id
  "Returns an UUID identifying the message."
  []

  (uuid/make-random-uuid))

(defn connect!
  "Connects to the server-side."
  []

  (when-not @client-id
    (reset! client-id (make-client-id)))

  ;(go
  ;(<! (async/timeout 3000)) ; just to test whether queuing works
  (when-not @sente-async
    (let [client-id @client-id
          packer (sente-transit/->TransitPacker :json {:handlers transit-utils/transit-write-handlers}
                   {:handlers transit-utils/transit-read-handlers})
          sente (sente/make-channel-socket! "/sente" {:type   :auto
                                                      :packer packer
                                                      :host "localhost:3000"
                                                      :client-id (uuid/uuid-string client-id)})
          sente-receive-chan (:ch-recv sente)
          sente-send-fn      (:send-fn sente)
          stop-router! (sente/start-chsk-router! sente-receive-chan handle-receive)
          stop-send-loop! (run-send-loop! sente-send-fn)
          disconnect-fn (fn []
                          (stop-send-loop!)
                          (stop-router!)
                          (async/close! sente-receive-chan))
          sente-data {:disconnect! disconnect-fn}]
      (reset! sente-async sente-data))));)

(defn disconnect!
  "Disconnects the async client."
  []

  (when @sente-async
    (let [{:keys [disconnect!]} @sente-async]
      (disconnect!)
      (reset! sente-async nil))))

(defn make-message-id
  "Returns an UUID identifying the message."
  []

  (uuid/make-random-uuid))

(defn send!
  "Queues the message to be sent to the server."
  [event-name payload]

  (let [message-id (make-message-id)
        event [event-name {:message-id message-id :payload payload}]]
    (async/put! send-queue {:event event
                            :try 0})
    message-id))
