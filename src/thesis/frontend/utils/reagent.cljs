(ns thesis.frontend.utils.reagent
  (:require [reagent.ratom :as ratom]
            [taoensso.timbre :as log]))

(deftype Wrapper [deref-reaction deref-fn reset-fn]
  ratom/IReactiveAtom

  IDeref
  (-deref [this]
    (-deref deref-reaction))

  IHash
  (-hash [_]
    (hash deref-fn))

  IReset
  (-reset! [this newval]
    (reset-fn newval))

  ISwap
  (-swap! [this f]          (-reset! this (f @deref-reaction)))
  (-swap! [this f x]        (-reset! this (f @deref-reaction x)))
  (-swap! [this f x y]      (-reset! this (f @deref-reaction x y)))
  (-swap! [this f x y more] (-reset! this (apply f @deref-reaction x y more)))

  IPrintWithWriter
  (-pr-writer [a w opts]
    (ratom/pr-atom deref-reaction w opts "Wrapper:"))

  IWatchable
  (-notify-watches [this old new]
    (#'ratom/notify-w deref-reaction old new))
  (-add-watch [this key f]
    (#'ratom/add-w deref-reaction key f))
  (-remove-watch [this key]
    (#'ratom/remove-w deref-reaction key))

  ratom/IDisposable
  (dispose! [this]
    (ratom/dispose! deref-reaction)))

(defn wrap [deref-fn reset-fn]
  (let [deref-reaction (ratom/make-reaction deref-fn :auto-run true)]
    (Wrapper. deref-reaction deref-fn reset-fn)))
