(ns thesis.frontend.utils.log)

(defn make-console-appender
  "Returns a simple js/console appender for ClojureScript, or nil if no
  js/console exists."
  []
  (when-let [have-logger? (and (exists? js/console) (.-log js/console))]
    (let [have-warn-logger?  (.-warn  js/console)
          have-error-logger? (.-error js/console)
          have-debug-logger? (.-debug js/console)
          have-trace-logger? (.-trace js/console)
          level->logger {:fatal  (if have-error-logger? :error :info)
                         :error  (if have-error-logger? :error :info)
                         :warn   (if have-warn-logger?  :warn  :info)
                         :debug  (if have-debug-logger? :debug  :info)
                         :trace  (if have-trace-logger? :trace  :info)}]
      {:enabled?   true
       :async?     false
       :min-level  nil
       :rate-limit nil
       :output-fn  :inherit
       :fn
                   (fn [data]
                     (let [{:keys [level output-fn vargs_ instant]} data
                           vargs      (force vargs_)
                           ;[v1 vnext] (enc/vsplit-first vargs)
                           output     (into-array vargs #_(cons (str "[" instant "]") vargs))]

                       (case (level->logger level)
                         :fatal (.apply (.-error js/console) js/console output)
                         :error (.apply (.-error js/console) js/console output)
                         :warn  (.apply (.-warn  js/console) js/console output)
                         :debug (.apply (.-debug js/console) js/console output)
                         :trace (.apply (.-trace js/console) js/console output)
                         (.apply (.-log   js/console) js/console output))))})))
