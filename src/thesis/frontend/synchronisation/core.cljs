(ns thesis.frontend.synchronisation.core
  (:require-macros [thesis.frontend.synchronisation.core :as macros]
                   [cljs.core.async.macros :refer [go go-loop]])
  (:require [datascript.core :as d]
            [taoensso.timbre :as log]
            [cljs.core.async :as async :refer [<! >!]]
            [cljs-uuid-utils.core :as uuid]
            [thesis.frontend.async :as app-async]
            [thesis.common.utils.core :as utils :refer [zip]]
            reagent.core
            reagent.ratom))

(defmulti handle-event
  ""
  (fn [event & _] event))

(def db-type-map
  (atom {}))

(defn register-db-type! [name descriptor]
  (swap! db-type-map assoc name descriptor))

(macros/def-db-type User
  {})

(macros/def-db-type Note
  {})

(def queries-map
  (atom {}))

(defn register-query! [name descriptor]
  (swap! queries-map assoc name descriptor))

(def schema
  {:note/user {:db/valueType :db.type/ref}
   :aggregate/id {:db/unique :db.unique/identity }
   :user/id      {:db/unique :db.unique/identity }
   :note/id      {:db/unique :db.unique/identity }
   })

(def db
  (d/create-conn schema))

#_(d/transact! db [{:db/id                 -1
                  :user/id               1
                  :user/username         "Test user"
                  :aggregate/type :user}
                 {:db/id -2
                  :note/id 1
                  :note/title "Test title note"
                  :note/text "Test somber note"
                  :note/user -1
                  :note/mood :somber
                  :aggregate/type :note}
                 {:db/id -3
                  :note/id 2
                  :note/title "Test title note2"
                  :note/text "Test note2"
                  :note/user -1
                  :aggregate/type :note}

                 {:db/id -4
                  :user/id 2
                  :user/username "User2"
                  :aggregate/type :user}
                 {:db/id -5
                  :note/id 3
                  :note/title "Test title user2 note"
                  :note/text "User 2 somber note"
                  :note/user -4
                  :note/mood :somber
                  :aggregate/type :note}
                 {:db/id -6
                  :note/id 4
                  :note/title "Test title user2 note2"
                  :note/text "User 2 upbeat note"
                  :note/user -4
                  :note/mood :upbeat
                  :aggregate/type :note}])

(app-async/connect!)

(defn generate-query-id! []
  (uuid/make-random-uuid))

(defn make-sync-query! [{:keys [name query args]}]
  (let [query-id (generate-query-id!)
        unregister-fn (fn [] true)]
    (app-async/send! :synchronisation/query {:name name
                                             :query query
                                             :args args})
    unregister-fn))

(d/listen! db
  (fn [report]
    (let [tx-data (:tx-data report)]
      ;(log/error "CHANGES" report)
      ; TODO: actually decide which query caches require busting
      (doseq [[query-symbol {:keys [bust-cache!]}] @queries-map]
        ;(log/debug "BUSTING CACHE FOR" query-symbol)
        (bust-cache!)))))

(def test-query
  (macros/build-query [db ?user-id]
    (User [:username {:id #{?user-id}} {:notes (Note [:text])}])))

(defn test-query! [id]
  (let [result (test-query id)]
    (.log js/console result)
    result))

(def users-query
  (macros/build-query [db]
    (User [:username :id])))

(def user-query
  (macros/build-query [db ?user-id]
    (User [:username {:id #{?user-id}}])))

(def notes-query
  (macros/build-query [db ?user-id]
    (Note [:title :text :mood {:user #{?user-id}}])))

(defn namespaced [kw-ns]
  (fn [kw]
    (if (and kw (not (namespace kw)))
      (keyword (name kw-ns) (name kw))
      kw)))

;(defn add-note! [new-note]
;  (let [db-note ]
;    (d/transact! [db-note])))

(defn transact! [entity-type & transaction]
(let [tempid (atom 0)
      entity (@db-type-map entity-type)
      ;_ (log/error "ENTITY" entity)
      entity-name (:aggregate/type entity)
      add-namespace (let [add-namespace (namespaced entity-name)]
                      (fn [[k v]]
                        [(add-namespace k) v]))
      update-transaction-value (fn [value]
                                 (cond
                                   (map? value) (into {} (conj (map add-namespace value)
                                                           [:db/id (swap! tempid dec)]
                                                           [:aggregate/type entity-name]))))
      transaction (mapv update-transaction-value transaction)]
  ;(log/debug "WILL TRANSACT" transaction)
  ; TODO: this needs to queue to backedn
  (d/transact! db transaction)))

(defn dispatch-event! [event & args]
  (log/debug "Handling event " event " with args " args)
  (apply handle-event event args))

(defmethod handle-event :add-note [_ {:keys [user title mood text] :as note}]
  (log/debug "WOULD ADD NOTE" note)
  (let [note-params {:title title
                     :mood mood
                     :text text
                     :user (:db/id user)}
        new-note (merge note-params {:db/id -1})
        transaction-result (transact! 'Note new-note)
        db-id (get (:tempids transaction-result) -1)]
    (app-async/send! :command/execute {:command :note/create
                                       :params (assoc note-params :user (:id user))
                                       :clientside-id db-id})
    (assoc new-note :db/id db-id)))

;(defmethod app-async/async-handler :synchronisation/update [event payload & metadata]
;  (doseq [entity (:result payload)]
;    (let [aggregate-type (:aggregate/type entity)
;          query '[:find ?e .
;                  :in $
;                  :where ['?e :aggregate/type aggregate-type]
;                         ['?e :aggregate/id (:aggregate/id entity)]]
;          existing-eid (d/q query
;                         @db)]
;      (if-not existing-eid
;        (transact! aggregate-type entity)
;        (transact! aggregate-type entity)))))


(defmethod app-async/async-handler :synchronisation/update [event payload & metadata]
  ;(log/debug "UPDATE IS COMING" event payload metadata)
  (doseq [entity (:result payload)]
    (let [aggregate-type (:aggregate/type entity)
          query (conj
                  '[:find ?e .
                    :in $
                    :where]
                  ['?e :aggregate/type aggregate-type]
                  ['?e :aggregate/id (:aggregate/id entity)])
          ;_ (log/debug "QUERYYY" query)
          existing-eid (d/q query
                         @db)
          ;_ (log/debug "EXISTING ID" existing-eid)
          ]
      ;(log/debug "ENTITY" entity)
      ;(log/debug "TRANSACTION DATA" aggregate-type [entity])
      ;(log/debug "WE GOT STH?" existing-eid)
      (if-not existing-eid
        ;(log/debug "NEW STUFF, MERGE IT")
        (transact! aggregate-type entity)
        (transact! aggregate-type entity) ; TODO: should updates be treated differently?
        )
      ))
  #_(log/debug "UPDATE IS DONE" event payload metadata))

(defn kek1 []
  @(notes-query [:user/id #uuid "ba0fc831-c919-11e5-af4b-753e477beb2f"]))

(defn kek2 []
  (d/q '[:find [?e ...]
         :in $ ; [?uid ...]
         :where [?e :aggregate/type :note]
                [?e :note/user [:aggregate/id #uuid "ba0fc831-c919-11e5-af4b-753e477beb2f"] #_?uid]]
    @db
    #{#uuid "ba0fc831-c919-11e5-af4b-753e477beb2f"}))

(defn kek3 []
  @(user-query #uuid "ba0fc831-c919-11e5-af4b-753e477beb2f"))

(defn kek4 []
  (transact! 'Note {:aggregate/id #uuid "038deec1-c978-11e5-977d-6eb2d284b215"
                    :aggregate/type :note
                    :note/title "Test title note"
                    :note/text "Lorem ipsum dolor sit amet"
                    :note/mood :dooty}))

(defn kek5 []
  @(users-query))