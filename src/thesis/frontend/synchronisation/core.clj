(ns thesis.frontend.synchronisation.core
  (:require [datascript.core :as d]
            [n01se.seqex :as se]
            [cuerdas.core :as str]
            [thesis.common.utils.core :as utils :refer [zip]]))

(def db-type-map
  (atom {}))

(defmacro def-db-type [type-name descriptor]
  (let [db-type-name         (keyword (str/lower (name type-name)))
        merged-descriptor (merge descriptor {:aggregate/type db-type-name})]
    (swap! db-type-map assoc type-name merged-descriptor)
    `(do
       (def ~type-name ~merged-descriptor)
       (thesis.frontend.synchronisation.core/register-db-type! '~type-name ~type-name))))

(declare query-seqex)

(def variable-seqex
  (se/cap (se/and #(symbol? %)
            #(str/starts-with? (name %) "?"))
    (fn [name] {:variables #{(first name)}})))

(def entity-name-seqex
  (se/cap symbol?
    (fn [name]
      {:type (first name)})))

(def field-seqex
  (se/cap keyword? #_(se/alt keyword? variable-seqex)
    (fn [name]
      {:fields name})))

(def constraint-seqex
  (se/cap
    (se/and map?
      ;(se/apply-fn (comp first first) (se/alt keyword? variable-seqex))
      (comp keyword? first first)
      (se/apply-fn (comp second first)
        (se/and set? (se/rep+ (se/subex variable-seqex)
                       (se/subex (comp not symbol?))))))
    (fn [name] {:constraints name})))
(def join-seqex
  (se/cap (se/and map?
            (comp keyword? first first)
            (comp list? second first))
    (fn [name] {:joins  name})))

(def options-seqex
  (se/and vector? (se/subex (se/rep* field-seqex constraint-seqex join-seqex))))

(def query-seqex (se/cat entity-name-seqex options-seqex))

(defn merge-fn [old new]
  (cond
    (set? old) (set (concat old new))
    :else      (vec (concat old new))))

(defn namespaced [kw-ns]
  (fn [kw]
    (if (and kw (not (namespace kw)))
      (keyword (name kw-ns) (name kw))
      kw)))

(defn- build-constraint-query [type-descriptor constraints]
  (let [type (:aggregate/type type-descriptor)
        wrap-namespace (namespaced type)
        id-sym (gensym (str "?" (name type) "-eid"))
        constraint-argument-names (map (fn [[field _]]
                                         (gensym (str "?" (namespace field) "-" (name field))))
                                    constraints)
        constraint-wheres (map (fn [[field _] constraint-argument]
                                 `[~id-sym ~(wrap-namespace field) ~constraint-argument])
                            constraints constraint-argument-names)]
    `[:find [~id-sym ...]
      :in ~'$ ~@(map (fn [argname] `[~argname ...] ) constraint-argument-names)
      :where [~id-sym :aggregate/type ~type]
      ~@constraint-wheres]))

(declare build-query*)

(defn build-join-queries [type-name joins opts]
  (when (some? joins)
    (let [id-sym (gensym (str "?" (name type-name) "-eid"))]
      (for [[join-key join-query] joins]
        (let [join-query-with-constraint# (let [[joined-type fields] join-query]
                                            (list joined-type (conj fields {type-name ^:-synchronisation/join #{id-sym}})))
              entity-gensym# (gensym "entity")]
          (cljs.util/debug-prn "JOIN QUERY " join-query-with-constraint#)
          `(map (fn [{~id-sym :db/id :as ~entity-gensym#}]
                  ~(let [query-descriptor (build-query* join-query-with-constraint# opts)
                         query-fragment# (:query-code-fragment query-descriptor)]
                     `(assoc ~entity-gensym# ~join-key ~query-fragment#)))))))))

(defn build-filter-result-fields-fragment [type-name fields]
  `(mapv (comp
           #(into {} (map (fn [[k# v#]]
                            [(if-not (= (namespace k#) "db")
                               (keyword (name k#))
                               k#) v#]) %)) ;; strips back the type prefix
           #(select-keys % ~(conj fields (keyword (name type-name) "id") :db/id)))))

(defn build-query* [query {:keys [curr-db-sym argument-map db-type-map-sym]:as opts}]
  (let [parsed
                         (binding [*out* *err*]
                           (locking cljs.util/debug-prn-mutex
                             (let [result (apply merge-with merge-fn (se/parse query-seqex query))]
                               (flush)
                               result)))
        argument-map (or argument-map {})
        type-symbol (:type parsed)
        type-descriptor (@db-type-map type-symbol)
        _ (cljs.util/debug-prn "TYPE MAP " @db-type-map)
        _ (cljs.util/debug-prn "TYPE DESCRIPTOR " type-descriptor)
        type-name (:aggregate/type type-descriptor) ; (str/lower (name (:-aggregate/type type-descriptor)))
        _ (cljs.util/debug-prn "TYPE NAME " type-name)
        wrap-namespace (namespaced type-name)
        fields (map wrap-namespace (:fields parsed))
        _ (cljs.util/debug-prn "CONSTRAINTS " (:constraints parsed))
        constraints (map (comp (juxt (comp wrap-namespace first) second) first) (:constraints parsed))
        joins (map first (:joins parsed))
        ;joins (:joins parsed)
        _ (cljs.util/debug-prn "JOINS " (map first joins))
        merged-fields (vec (concat fields
                             (keep (fn [[field constraint]]
                                     (when-not (:-synchronisation/join (meta constraint))
                                       field)) constraints)
                             (map first joins)))
        _ (cljs.util/debug-prn "MERGED " merged-fields)
        ; id-sym (gensym (str "?" (name type-name) "-eid"))
        query-arguments (mapv (fn [[_ constraint]]
                                ; (cljs.util/debug-prn "MAPPING CONSTRAINT= " constraint)
                                (into #{} (map #(or (when-let [argument (argument-map %)] ; warn on missing variables
                                                      `@~argument)
                                                 %)
                                            constraint)))
                          constraints)
        constraint-query (build-constraint-query type-descriptor constraints)
        join-queries (build-join-queries type-name joins opts)
        query `(let [rt-type-descriptor# (~db-type-map-sym ~type-symbol)
                     ;rt-type-name# (:aggregate/type rt-type-descriptor#)
                     query# '~constraint-query
                     query-arguments# ~query-arguments
                     ] ;[~curr-db-sym ~db]
                 (log/debug "RUNNING QUERY: " query# " WITH ARGUMENTS" query-arguments#)
                 (->> (apply d/q
                        query#
                        ~curr-db-sym
                        query-arguments#)
                   (map (fn [db-id#]
                          (assoc
                           (->> (d/entity ~curr-db-sym db-id#)
                             (into {}))
                            :db/id db-id#)))
                   ~@join-queries
                   ~(build-filter-result-fields-fragment type-name merged-fields)))]
    ; (cljs.util/debug-prn parsed)
    {:query-code-fragment query}))

(defmacro build-query
  ([[db & argnames] query]
   (cljs.util/debug-prn "\n===\n")
   (cljs.util/debug-prn query)
   (cljs.util/debug-prn db)
   (let [db-type-map-sym (gensym "db-type-map")
         curr-db-sym (gensym "curr-db")
         arg-gensyms# (into {} (map (juxt identity (comp gensym str)) argnames))
         query-descriptor (build-query* query {:curr-db-sym curr-db-sym
                                               :argument-map arg-gensyms#
                                               :db-type-map-sym db-type-map-sym})

         query-fragment# (:query-code-fragment query-descriptor)
         cached-query-sym (gensym "cached-query")
         run-query!-sym (gensym "run-query!")
         query-name (gensym "query-name")
         register-query!# 'thesis.frontend.synchronisation.core/register-query!
         make-sync-query!# 'thesis.frontend.synchronisation.core/make-sync-query!
         resulting-query# `(let [~db-type-map-sym 'thesis.frontend.synchronisation.core/db-type-map
                                 ~@(mapcat (fn [[_ argname]]
                                             [argname '(atom nil)])
                                     arg-gensyms#)
                                 ~run-query!-sym (fn []
                                                   (let [~curr-db-sym @~db
                                                         result# ~query-fragment#]
                                                     result#))
                                 ~cached-query-sym (reagent.core/atom #_(~run-query!-sym)
                                                     ~(when (empty? argnames)
                                                        `(~run-query!-sym)))
                                 bust-query-cache-fn# #(reset! ~cached-query-sym nil)
                                 query-info#     {:bust-cache! bust-query-cache-fn#}
                                 dispose-callback# (fn []
                                                     (log/error "!! WILL UNREGISTER !!" '~query-name))
                                 query-reaction# (reagent.ratom/make-reaction
                                                   (fn []
                                                     (if-let [cached-query# @~cached-query-sym]
                                                       cached-query#
                                                       (reset! ~cached-query-sym (~run-query!-sym))))
                                                   :on-dispose dispose-callback#)]
                             (~register-query!# '~query-name query-info#)
                             (fn [~@argnames]
                               (let []
                                 (~make-sync-query!# {:name '~query-name
                                                      :query '~query
                                                      :args (into {} (zip ~(mapv (fn [arg] `'~arg) argnames) [~@argnames]))})
                                 (taoensso.timbre/debug "WILL RESET" ~@(map name argnames) "WITH" ~@argnames)
                                 ~@(map (fn [argname]
                                          (let [arg-symbol# (get arg-gensyms# argname)]
                                            `(when (not= @~arg-symbol# ~argname)
                                               (reset! ~arg-symbol# ~argname)
                                               (reset! ~cached-query-sym (~run-query!-sym)))))
                                     argnames)
                                 query-reaction#)))]
     (cljs.util/debug-prn "RESULTING QUERY\n" (with-out-str (clojure.pprint/pprint resulting-query#)))
     (cljs.util/debug-prn "\n===\n")
     resulting-query#)))

#_(build-query (User. [{:id 1} {:notes (Note. [:text])}]))

