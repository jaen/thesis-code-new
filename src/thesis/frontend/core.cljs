(ns thesis.frontend.core
  (:require [devtools.core :as devtools]
            [reagent.core :as reagent]
            [taoensso.timbre :as log]
            [goog.events :as gevents]

            [thesis.frontend.application :as app]
            [thesis.frontend.utils.log :as utils-log]
            [thesis.frontend.synchronisation.core])
  (:import [goog.events EventType]))

(defn install-devtools! []
  (devtools/set-pref! :install-sanity-hints true)
  (devtools/enable-feature! :dirac)
  (devtools/install!))

(defn mount-root! []
  (reagent/render [app/application-component] (. js/document (querySelector ".app-container"))))

(defn app-init! []
  (install-devtools!)
  (when-let [appender (utils-log/make-console-appender)]
    (log/merge-config! {:appenders {:console appender}}))
  (log/debug "starting app")
  (mount-root!)
  #_(history/setup-history! (.-body js/document)))

(defn app-reload! []
  (log/debug "reloading app")
  (mount-root!))
