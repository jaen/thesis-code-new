(ns thesis.frontend.application
  (:require-macros [reagent.ratom :as ratom]
                   [cljs.core.async.macros :as async-macros :refer [go go-loop]])
  (:require [reagent.core :as reagent]
            [cuerdas.core :as str]
            [cljs-uuid-utils.core :as uuid]
   ;[cljs.core.async :as async :refer [<! >!]]
            [hodgepodge.core :as storage]
            [taoensso.timbre :as log]
            [thesis.frontend.async :as async]
            [thesis.frontend.synchronisation.core :as sc]
            [thesis.frontend.utils.reagent :as reagent-utils]
   #_[reagent.ratom :as ratom]))



(def selected-user-id (reagent/atom nil))
(def selected-user (reagent.ratom/reaction
                     (log/debug "SELECTED USER ID= " selected-user-id)
                     (let [user (first @(sc/user-query @selected-user-id))]
                       (log/debug "USER= " user)
                       user)))

(defn show-user-component [user]
  (let [] ;[user @user]
    (log/debug "SHOW USER REPAINT")
    [:div.test
     [:b "Username"] [:br]
     (:username user) [:br]
     [:b "User notes"] [:br]
     [:ul
      (doall (for [note (:notes user)]
               ^{:key (str "note-" (:id note))}
               [:li (:text note)]))]]))

(defn user-component []
  (let [user-id (reagent/atom 1)
        ;user (ratom/run! (first @(sc/test-query @user-id)))
        ]
    (fn []
      (let [user (first @(sc/test-query @user-id))]
        (log/debug "USER REPAINT" user-id)
        (log/debug "USER " user)
        [:div.topek
         "User id: " @user-id
         [:a {:href "#" :on-click (fn [e]
                                    (swap! user-id inc)
                                    (.preventDefault e))}
          "UP"]
         [:a {:href "#" :on-click (fn [e]
                                    (swap! user-id dec)
                                    (.preventDefault e))}
          "DOWN"]
         [:br]
         [show-user-component user]]))))

(defn counter-component []
  (let [counter (reagent/atom 0)]
    (fn []
      (log/debug "COUNTER REPAINT")
      [:div.lel
       "Test: " @counter
       [:br]
       [:a {:href "#" :on-click (fn [e]
                                  (swap! counter inc)
                                  (.preventDefault e))}
        "Increment me!"]
       [:br]
       [user-component]])))

(defn list-notes-component []
  (let [notes (ratom/reaction
                (when-let [user-id (:id @selected-user)]
                  (log/debug "!USER=" @selected-user)
                  (log/debug "USER DB/ID=" user-id)
                  @(sc/notes-query [:user/id user-id])))]
    (fn []
      (log/debug "NOTES=" @notes)
      [:table.ui.striped.table
       [:thead
        [:tr
         [:th {:style {:width "25%"}} "Title"]
         [:th {:style {:width "15%"}}"Mood"]
         [:th "Text"]]]
       [:tbody
        (doall (for [note @notes]
                 ^{:key (str "note-" (:db/id note))}
                 [:tr
                  [:td (:title note)]
                  [:td (when-let [mood (:mood note)]
                         (name mood))]
                  [:td (:text note)]]))]])))

(defn dropdown [{:keys [value] :as options} choices]
  (reagent/create-class
    {:component-did-mount
     (fn [component]
       ;(log/error "component did mount!")
       (let [dom-node (reagent/dom-node component)]
         (.dropdown (js/$ dom-node) #js {:match    "text"
                                         :onChange (fn [new-value]
                                                     (reset! value new-value))})))

     :component-did-update
     (fn [component]
       ;(log/error "component did update!" @value)
       (let [dom-node (reagent/dom-node component)]
         (.dropdown (js/$ dom-node) "set selected" @value)
         (.dropdown (js/$ dom-node) "refresh")))

     :reagent-render
     (fn [{:keys [value] :as options} choices]
       (log/error "OPTIONS = " options)
       (let [_ @selected-user-id
             value @value]
         [:div.ui.fluid.search.selection.dropdown
          [:input {:type "hidden" :name "user" :value value}]
          [:i.dropdown.icon]
          [:div.default.text "Select User"]
          [:div.menu
           ; (log/debug "USERS=" @(sc/users-query))
           (doall (for [{:keys [value text]} choices]
                    (conj
                      ^{:key (str "choice-" value)}
                      [:div.item {:data-value value}]
                      text)))]]))}))

(defn add-note-component []
  (let [note (reagent/atom {})
        mood (reagent-utils/wrap #(:mood @note) #(swap! note assoc :mood %))]
    (fn []
      ;(log/debug "CURRENT NOTE=" @note)
      [:form.ui.form.segment
       [:div.field
        [:label "Note Title"]
        [:input {:type "text" :value (:title @note) :on-change #(swap! note assoc :title (.. % -target -value))}]]
       [:div.field
        [:label "Note Mood"]
        [dropdown {:value mood}
         [{:value "sad" :text "Sad"}
          {:value "happy" :text "Happy"}]]]
       [:div.field
        [:label "Note Text"]
        [:textarea {:on-change #(swap! note assoc :text (.. % -target -value))}
         (:text @note)]]
       [:div.ui.submit.primary.button.fluid {:on-click (fn [e]
                                                         ;(log/debug "CURRENT NOTE=" @note)
                                                         (let [note (merge @note
                                                                      {:user @selected-user})]
                                                           (sc/dispatch-event! :add-note note))
                                                         (.preventDefault e))}
        "Add Note"]])))

(defn user-choice-component []
  (let [user (reagent-utils/wrap
               #(:id @selected-user)
               #(reset! selected-user-id (uuid/make-uuid-from %)))]
    (fn []
      (let [users (sc/users-query)]
        #_(log/debug "USERS QUERY: " @users)
        #_(log/debug "KEK" (for [user @users]
                           (do (log/debug "USER! " user)
                               {:value (:id user) :text (:username user)})))
        [dropdown {:value     user}
         (for [user @users]
           (do #_(log/debug "USER! " user)
               {:value (:id user) :text (:username user)}))]))))

(defn page-component []
  [:div.ui.grid
   [:div.sixteen.wide.column
    [user-choice-component]]
   [:div.sixteen.wide.column
    [:div.ui.grid
     [:div.twelve.wide.column
      [list-notes-component]
      #_[counter-component]]
     ;[:div.ui.vertical.divider]
     [:div.four.wide.column
      [add-note-component]]]]])

(defn application-component []
  [:div.application.ui.container
   [:div.ui.top.fixed.menu
    [:div.header.item "Test"]]
   [:div
    [page-component]]])
