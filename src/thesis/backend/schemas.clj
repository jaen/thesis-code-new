(ns thesis.backend.schemas
  (:require [schema.core :as s]
            [thesis.common.utils.schema :as schema-utils]
            [cats.monad.maybe :as maybe]
            [taoensso.timbre :as log])
  (:import [java.util UUID]
           [org.joda.time DateTime]))

;(defn maybe [schema]
;  (s/conditional
;    maybe/just?    (s/pred)
;    maybe/nothing? (s/either nil maybe/Nothing)))

(def AggregateType
  (s/enum :user :note))

(def EventType
  s/Keyword
  #_(s/constrained s/Keyword (fn [value]
                             (not (s/check AggregateType (keyword (namespace value)))))))

(def EventVersion
  s/Int)

(def EventPayload
  {s/Any s/Any})

(def EventID
  s/Uuid)

(def Timestamp
  DateTime)

(def AggregateVersion
  s/Int)

(def AggregateID
  s/Uuid)

(def test-dispatch-fn
  (fn [map]
    (log/debug "MUH MAP" map)
    ((juxt :aggregate/type :event/type :event/version) map)))

(def Event
  (schema-utils/abstract-map-schema
    #_(juxt :aggregate/type :event/type :event/version)
    test-dispatch-fn
    {:event/id           EventID
     :event/type         EventType
     :event/version      EventVersion
     :aggregate/version  AggregateVersion
     :aggregate/id       AggregateID
     :aggregate/type     AggregateType
     :event/timestamp    Timestamp
     :event/payload      EventPayload}))

(schema-utils/extend-schema UserCreatedEvent Event [[:user :created 1]]
  {;:event/type    :created
   :event/payload {:username s/Str
                   :password s/Str}})

(schema-utils/extend-schema UserUpdatedPersonalInfoEvent Event [[:user :updated-personal-info 1]]
  {;:event/type    :created
   :event/payload {:username s/Str}})

(schema-utils/extend-schema NoteCreatedEvent Event [[:note :created 1]]
  {;:event/type    :created
   :event/payload {:user s/Uuid
                   :title s/Str
                   :text s/Str
                   :mood s/Keyword}})

(schema-utils/extend-schema NoteUpdatedEvent Event [[:note :updated 1]]
  {;:event/type    :created
   :event/payload {(s/optional-key :user) s/Uuid
                   (s/optional-key :title) s/Str
                   (s/optional-key :text) s/Str
                   (s/optional-key :mood) s/Keyword}})

(def Aggregate
  (schema-utils/abstract-map-schema
    :aggregate/type
    {:aggregate/id         AggregateID
     :aggregate/version    AggregateVersion
     :aggregate/type       AggregateType
     :aggregate/created-on Timestamp
     :aggregate/updated-at Timestamp}))

(schema-utils/extend-schema UserAggregate Aggregate [:user]
  {:aggregate/fields {:username s/Str
                      :password s/Str}})

(schema-utils/extend-schema NoteAggregate Aggregate [:note]
  {:aggregate/fields {:user s/Uuid
                      :title s/Str
                      :text s/Str
                      :mood s/Keyword}})
