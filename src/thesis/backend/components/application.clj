(ns thesis.backend.components.application
  (:require [com.stuartsierra.component :as component]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.params :refer [wrap-params]]

            [thesis.common.utils.core :refer :all]
            [thesis.backend.components.protocols :as protocols]))

(defrecord Application [handler async]
  component/Lifecycle
    (start [component]
      component)

    (stop [component]
      component)

  protocols/WebServiceProvider
    (-get-routes [_]
      (concatv []
        (some-> async protocols/-get-routes)
        [["test" :application/test]]))

    (-get-handlers [_]
      (merge {}
        (some-> async protocols/-get-handlers)
        {:application/test (fn [req]
                             {:status  200
                              :headers {"Content-Type" "text/html"}
                              :body    (str "test")})})))

(defn make [& [options]]
  (map->Application {}))
