(ns thesis.backend.components.logic
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]

            [thesis.common.utils.core :refer :all]
            [thesis.backend.components.protocols :as protocols]
            [thesis.backend.components.command-dispatcher :as command-dispatcher]))

(defrecord Logic [command-dispatcher repository]
  component/Lifecycle
    (start [component] component)
    (stop [component] component)

  protocols/Logic
    (-handle-command! [this command params]
      (protocols/run-command! command-dispatcher command params))

    (-query-view [this query]
      ))

(defn make [& [options]]
  (map->Logic {}))
