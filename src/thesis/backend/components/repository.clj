(ns thesis.backend.components.repository
  (:require [com.stuartsierra.component :as component]
            ;[clj-uuid :as uuid]
            [clojure.core.async :as async :refer [<! >! go-loop]]
            [cats.monad.either :as either]
            [cats.monad.maybe :as maybe]
            [clojure.java.io :as io]
            [taoensso.timbre :as log]
            [cats.core :as m]

            [thesis.backend.components.event-store :as event-store]
            [thesis.common.utils.transit :as transit-utils]
            [thesis.backend.components.protocols :as protocols])
  (:import [clojure.lang MultiFn]))

(defn- make-dispatch-method []
  (new MultiFn "thesis.backend.components.repository/event-dispatch-method"
    (fn [_ {aggregate :aggregate/type event :event/type version :event/version} _]
      [aggregate event version])
    :default
    #'clojure.core/global-hierarchy))

(defn- make-initial-aggregate [first-event]
  {:aggregate/id         (:aggregate/id first-event)
   :aggregate/version    1
   :aggregate/type       (:aggregate/type first-event)
   :aggregate/created-on (:event/timestamp first-event)
   :aggregate/updated-on (:event/timestamp first-event)
   :aggregate/fields      {}})

(defn- fold-single-aggregate [dispatch-method events]
  (let [state (atom {})]
    (m/foldm
      (fn [aggregate event]
        (let [new-fields (dispatch-method
                           (:aggregate/fields aggregate)
                           event state)]
          (m/<$> (fn [fields]
                   (merge
                     (update aggregate :aggregate/version inc)
                     {:aggregate/updated-on (:event/timestamp event)
                      :aggregate/fields     fields}))
            new-fields)))
      (make-initial-aggregate (first events))
      events)))

(defn- fold-aggregates [dispatch-method grouped-events]
  (reduce
    (fn [acc [_ events]]
      (let [aggregate (fold-single-aggregate dispatch-method events)]
        (cond
          (maybe/just? aggregate)
            (conj acc (maybe/from-maybe aggregate))
          :else
            acc)))
    []
    grouped-events))

(defrecord Repository [event-store aggregate-cache dispatch-method]
  component/Lifecycle
    (start [component]
      (let [dispatch-method (make-dispatch-method)
            event-handlers  (filter #(satisfies? protocols/DomainEventProvider %) (vals component))
            handlers        (mapcat protocols/get-event-handlers event-handlers)]
        (doseq [[dispatch-value handler] handlers]
          (log/debug "Registering event handler for" dispatch-value)
          (.addMethod ^MultiFn dispatch-method dispatch-value handler))
        (.addMethod ^MultiFn dispatch-method :default (fn [_ event _] (log/debug "Unknown event: " event)))
        (assoc component :dispatch-method dispatch-method)))

      (stop [component]
      (dissoc component :dispatch-method))

  protocols/Repository
    (-retrieve [this filter]
      (m/mlet [events (protocols/retrieve-events event-store filter)]
        (let [aggregates (->> (group-by :aggregate/id events) ; filter can return one or multiple
                              (fold-aggregates dispatch-method))]
          (cond
            (= (count aggregates) 1) (maybe/just (first aggregates))
            (not-empty aggregates)   (maybe/just aggregates)
            :else                    (maybe/nothing))))))

(defn make [& [options]]
  (map->Repository {}))
