(ns thesis.backend.components.view-store
  (:require [com.stuartsierra.component :as component]
   ;[clj-uuid :as uuid]
            [clojure.core.async :as async :refer [<! >! go go-loop]]
            [cats.monad.either :as either]
            [cats.monad.maybe :as maybe]
            [clojure.java.io :as io]
            [taoensso.timbre :as log]
            [cats.core :as m]
            [clj-uuid :as uuid]

            [thesis.backend.components.event-store :as event-store]
            [thesis.common.utils.transit :as transit-utils]
            [thesis.backend.components.protocols :as protocols])
  (:import [clojure.lang MultiFn]))

(defn- compare-with-filter [filter-val to]
  (cond
    (map? filter-val) (every? (fn [[key subfilter]]
                                (compare-with-filter subfilter (get to key)))
                        filter-val)
    ; (vector? filter-val) (= filter-val to)
    (or
      (fn? filter-val)
      (set? filter-val))    (filter-val to)
    :else                (= filter-val to)))

#_(defn- make-filter [filter]
  (cond
    (= :event/any-event filter)
    (fn [_]
      true)
    :else
    (let [event-filter-keys  (keys filter)
          payload-filter-keys (keys (:aggregate/fields filter))
          extract-comparison-map (fn [event]
                                   (let [map-subset (select-keys event event-filter-keys)
                                         payload-subset (select-keys (:event/payload event)
                                                          payload-filter-keys)]
                                     (merge map-subset (when (seq payload-subset)
                                                         {:event/payload payload-subset}))))]
      (fn [event]
        (let [filter-map     (clojure.set/rename-keys filter {:aggregate/fields :event/payload})
              comparison-map (extract-comparison-map event)]
          (compare-with-filter filter-map comparison-map))))))

; just simple equality for now.
; UGH DUPLICATION
(defn- make-filter [filter-map]
  (cond
    (= :view/any filter-map)
      (fn [_]
        true)
    :else
      (let [view-filter-keys   (keys filter-map)
            extract-comparison-map (fn [view]
                                     (select-keys view view-filter-keys))]
      (fn [view]
        ;(log/debug "VIEW" view)
        ;(log/debug "COMP" (extract-comparison-map view))
        (let [comparison-map (extract-comparison-map view)]
          (compare-with-filter filter-map comparison-map))))))

(defn- make-dispatch-method []
  (new MultiFn "thesis.backend.components.view-store/view-dispatch-method"
    (fn [view-name _ _]
      view-name)
    :default
    #'clojure.core/global-hierarchy))

(defn- read-cache! [path]
  (if (.exists (io/file path))
    (with-open [input-stream (io/input-stream path)]
      (transit-utils/read :msgpack input-stream))
    {}))

(defn- store-cache! [path events]
  (io/make-parents path)
  (with-open [output-stream (io/output-stream path)]
    (transit-utils/write :msgpack output-stream events)))

(defn- make-subscription-identifier []
  (uuid/v1))

(defn- notify-about-view-update! [view subscriptions]
  (when-let [subscriptions (seq subscriptions)]
    (go-loop [[[_ {:keys [channel matches?]}] & rest] subscriptions]
      (when (matches? view)
        (>! channel view))
      (when rest
        (recur rest)))))

(defn- run-update-loop! [dispatch-value {:keys [handlers subscriptions
                                                subscription-chan view-cache
                                                repository]}]
  (go-loop []
    (when-let [event (<! subscription-chan)]
      (log/debug "Would use event " event " to update " dispatch-value " view.")
      (let [{:keys [aggregate/type aggregate/id]} event
            matching-views (filter #((:filter-fn %) event) @handlers)
            view-results   (map (fn [{:keys [view-fn] :as view}]
                                  (let [result  (view-fn dispatch-value
                                                  (:aggregate/id event)
                                                  repository)]
                                    (log/debug "WILL UPDATE THIS: " result)
                                    [view (m/extract result)]))
                             matching-views)
            new-views      (filter (comp some? second) view-results)]
        (log/debug "HEY FRONTEND LOOK AT THIS AGGREGATE=" type " UUID=" id)
        (log/debug "HANDLERS:" (count @handlers))
        (log/debug "MATCHING:" (vec view-results))
        (log/debug "NEW:" (vec new-views))

        (doseq [[view view-result] new-views]
          (log/debug "VN=" (:view-name view) "ID=" (:aggregate/id event) "VR=" view-result)

          (notify-about-view-update! view-result @subscriptions)
          (swap! view-cache update-in [(:view-name view)] assoc (:aggregate/id event) view-result)))
      (recur))
    #_(log/debug "Stopping view update for " dispatch-value)))

(defrecord ViewStore [event-store repository view-cache dispatch-method
                      subscriptions storage-path event-subscriptions handlers]
  component/Lifecycle
    (start [component]
      (let [dispatch-method     (make-dispatch-method)
            event-handlers      (filter #(satisfies? protocols/ViewHandlerProvider %) (vals component))
            event-subscriptions (atom #{})
            subscriptions       (atom {})
            view-cache          (atom (read-cache! storage-path))
            handlers            (atom [])]
        (doseq [[dispatch-value
                 {filter-map :filter
                  :keys [view-fn] :as handler-descriptor}] (mapcat protocols/get-view-handlers
                                                                   event-handlers)]
          (log/debug "Registering event handler for" dispatch-value)
          (swap! handlers conj {:view-name dispatch-value
                                :filter-fn (#'event-store/make-filter filter-map)
                                :view-fn   view-fn})
          ;; REGISTERING SUBSCRIPTIONS
          (let [[subscription-uuid subscription-chan] (protocols/subscribe! event-store filter-map)]
            (log/debug "Subscribed for updates under " subscription-uuid)
            (swap! event-subscriptions conj subscription-uuid)
            (run-update-loop! dispatch-value {:handlers          handlers
                                              :subscriptions     subscriptions
                                              :subscription-chan subscription-chan
                                              :view-cache        view-cache
                                              :repository        repository})

            (.addMethod ^MultiFn dispatch-method dispatch-value view-fn)))
        (.addMethod ^MultiFn dispatch-method :default (fn [view-name _ _]
                                                        (log/debug "Unknown view: " view-name)))
        (assoc component :dispatch-method     dispatch-method
                         :subscriptions       subscriptions
                         :event-subscriptions event-subscriptions
                         :view-cache          view-cache
                         :handlers            handlers)))

    (stop [component]
      (when event-subscriptions
        (doseq [subscription-uuid @event-subscriptions]
          (log/debug "Unsubscribing " subscription-uuid)
          (protocols/unsubscribe! event-store subscription-uuid)))
      (store-cache! storage-path @view-cache)
      (dissoc component :dispatch-method :subscriptions :event-subscriptions :view-cache))

  protocols/ViewStore
    (-retrieve-view [this view-name view-id]
      (let [view-result (dispatch-method view-name view-id repository)]
        (m/<$> (fn [view]
                 (swap! view-cache update-in [view-name] assoc view-id view)
                 view)
          view-result)))

    ; TODO: unify those somehow?
    (-query-view' [this view-name query]
      ;(println "VIEW-NAME" view-name)
      ;(println "VIEW-CACHE" @view-cache)
      (if-let [candidates (get @view-cache view-name)]
        (let [results (filter (make-filter query) (vals candidates))]
          (log/debug "MUH RESULTS" (vec results))
          (if-not (empty? results)
            (maybe/just (vec results))
            (maybe/nothing)))
        (maybe/nothing)))

  protocols/Subscribable
    (-subscribe! [this {:keys [view-name filter] :as query}]
      (let [uuid (make-subscription-identifier)
            subscription-channel (async/chan)
            subscription {:channel  subscription-channel
                          :view-name view-name
                          :matches? (make-filter filter)
                          :filter   filter}]
        (swap! subscriptions assoc uuid subscription)
        [uuid subscription-channel]))

    (-unsubscribe! [this uuid]
      (log/debug "!KEEEEK!" @subscriptions)
      (let [{:keys [channel] :as kek} (get @subscriptions uuid)]
        (log/debug "!KEEEEK!" kek)
        (swap! subscriptions dissoc uuid)
        (async/close! channel)
        true)))

(defn make [& [options]]
  (map->ViewStore {:storage-path "data/views.store"}))

#_(reduce
    (fn [acc new]
      (println "ACC=" acc " NEW=" new)
      (println "KEK=" ((m/lift-m 2 concat) acc new))
      ((m/lift-m 2 concat) acc new))
    (maybe/just [])
    [(maybe/just [1 2]) (maybe/just [3 4])])