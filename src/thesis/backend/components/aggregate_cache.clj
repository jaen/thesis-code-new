(ns thesis.backend.components.aggregate-cache
  (:require [com.stuartsierra.component :as component]
            [clj-uuid :as uuid]
            [clojure.core.async :as async :refer [<! >! go-loop]]
            [cats.monad.either :as either]
            [cats.monad.maybe :as maybe]
            [clojure.java.io :as io]
            [taoensso.timbre :as log]
            [thesis.common.utils.transit :as transit-utils]
            [thesis.backend.components.protocols :as protocols]))

(defn read-snapshots! [path]
  (if (.exists (io/file path))
    (with-open [input-stream (io/input-stream path)]
      (transit-utils/read :msgpack input-stream))
    {}))

(defn store-snapshots! [path events]
  (io/make-parents "data")
  (with-open [output-stream (io/output-stream path)]
    (transit-utils/write :msgpack output-stream events)))

(defrecord AggregateCache [aggregate-cache storage-path]
  component/Lifecycle
    (start [component]
      (log/info "Loading snapshots from '" storage-path "'.")
      (let [initial-value (read-snapshots! storage-path)]
        (assoc component :aggregate-cache (atom initial-value))))

    (stop [component]
      (log/info "Storing snapshots to '" storage-path "'.")
      (store-snapshots! storage-path @aggregate-cache)
      (dissoc component :aggregate-cache))

  protocols/AggregateCache
    (-cache! [this aggregate]
      (let [uuid (:aggregate/id aggregate)]
        (swap! aggregate-cache assoc uuid aggregate)))

    (-retrieve! [this uuid]
      (if-let [cached-version (get @aggregate-cache uuid)]
        (maybe/just cached-version)
        (maybe/nothing)))

    (-evict! [this uuid]
      (swap! aggregate-cache dissoc uuid)))

(defn make [& [options]]
  (map->AggregateCache {:storage-path "data/snapshots.store"}))
