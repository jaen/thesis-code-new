(ns thesis.backend.components.webserver
  (:require [com.stuartsierra.component :as component]
            [immutant.web :as immutant]
            [taoensso.timbre :as log]
            [ring.middleware.stacktrace :as ring-stacktrace]
            [bidi.ring :as bidi-ring]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.params :refer [wrap-params]]

            [thesis.common.utils.core :refer :all]
            [thesis.backend.components.protocols :as protocols]))

(defn- build-ring-handler [routes handlers]
  (-> (bidi-ring/make-handler routes handlers)
      (wrap-keyword-params)
      (wrap-params)))

(defn- wrap-development [handler]
  (ring-stacktrace/wrap-stacktrace handler))

(defrecord WebServer [environment options server application]
  component/Lifecycle
    (start [component]
     (log/info "Starting webserver with: " options)
     (let [[routes handlers] (->> (vals component)
                                  (filter #(satisfies? protocols/WebServiceProvider %))
                                  (map (juxt protocols/-get-routes protocols/-get-handlers))
                                  (reduce (fn [[nums keys] [new-nums new-keys]]
                                            [(concatv nums new-nums) (merge keys new-keys)])
                                    [[] {}]))
           _ (log/debug "ROUTES: " ["/" routes])
           handler (build-ring-handler ["/" routes] handlers)
           wrapped-handler (if (= environment :development)
                             (wrap-development handler)
                             handler)
           server  (immutant/run wrapped-handler options)]
       (assoc component :server server)))

    (stop [component]
     (when server
       (log/info "Stopping webserver.")
       (immutant/stop server)
       component)))

(defn make [& [options]]
      (map->WebServer {:environment :development
                       :options     (merge {:host "0.0.0.0" :port 3000}
                                           options)}))

