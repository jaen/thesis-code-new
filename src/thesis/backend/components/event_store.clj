(ns thesis.backend.components.event-store
  (:require [com.stuartsierra.component :as component]
            [clj-uuid :as uuid]
            [clojure.core.async :as async :refer [<! >! go-loop]]
            [cats.monad.either :as either]
            [cats.monad.maybe :as maybe]
            [clojure.java.io :as io]
            [taoensso.timbre :as log]

            [thesis.common.utils.transit :as transit-utils]
            [thesis.backend.components.protocols :as protocols]
            [cats.core :as m]))

(defn- compare-with-filter [filter-val to]
  (cond
    (map? filter-val) (every? (fn [[key subfilter]]
                                (compare-with-filter subfilter (get to key)))
                        filter-val)
    ; (vector? filter-val) (= filter-val to)
    (or
      (fn? filter-val)
      (set? filter-val)) (filter-val to)
    :else                (= filter-val to)))

(defn- make-filter [filter]
  (cond
    (= :event/any-event filter)
    (fn [_]
      true)
    :else
    (let [event-filter-keys  (keys filter)
          payload-filter-keys (keys (:aggregate/fields filter))
          extract-comparison-map (fn [event]
                                   (let [map-subset (select-keys event event-filter-keys)
                                         payload-subset (select-keys (:event/payload event)
                                                          payload-filter-keys)]
                                     (merge map-subset (when (seq payload-subset)
                                                         {:event/payload payload-subset}))))]
      (fn [event]
        (let [filter-map     (clojure.set/rename-keys filter {:aggregate/fields :event/payload})
              comparison-map (extract-comparison-map event)]
          (compare-with-filter filter-map comparison-map))))))

(defn- make-subscription-identifier []
  (uuid/v1))

(defn- notify-about-event! [event subscriptions]
  (when-let [subscriptions (seq subscriptions)]
    ;(log/debug "Notifying subscriptions of new event: " event subscriptions)
    (go-loop [[[_ {:keys [channel matches? filter]}] & rest] subscriptions]
      ;(log/debug "Testing filter: " filter)
      (when (matches? event)
        ;(log/debug "  Subscription matches.")
        (>! channel event))
      (when rest
        (recur rest)))))

(defn- read-events! [path]
  (if (.exists (io/file path))
    (with-open [input-stream (io/input-stream path)]
      (transit-utils/read :msgpack input-stream))
    {}))

(defn- store-events! [path events]
  (io/make-parents path)
  (with-open [output-stream (io/output-stream path)]
    (transit-utils/write :msgpack output-stream events)))

(defrecord EventStore [index subscriptions storage-path]
  component/Lifecycle
    (start [component]
      (log/info "Loading events from '" storage-path "'.")
      (let [initial-value (read-events! storage-path)]
        (assoc component :index (atom initial-value)
                         :subscriptions (atom {}))))

    (stop [component]
      (log/info "Storing events to '" storage-path "'.")
      (store-events! storage-path @index)
      (dissoc component :index :subscriptions))

  protocols/EventStore
    (-append-event! [this event]
      (let [aggregate-id (:aggregate/id event)]
        (swap! index update-in [aggregate-id] (comp vec conj) event)
        (notify-about-event! event @subscriptions)
        (either/right event)))

    (-retrieve-events [this filter-desc]
      (if-let [aggregate-id (let [id (:aggregate/id filter-desc)]
                              (and (= (count filter-desc) 1)
                                   (not (or (fn? id)
                                            (set? id)))
                                   id))]
        (if-let [value (get @index aggregate-id)]
          (maybe/just value)
          (maybe/nothing))
        (do
          (if-let [events (seq (filter (make-filter filter-desc)
                                 (apply concat (vals @index))))]
            (maybe/just (sort-by :aggregate/version events))
            (maybe/nothing)))))

  protocols/Subscribable
    (-subscribe! [this filter]
      (let [uuid (make-subscription-identifier)
            subscription-channel (async/chan)
            subscription {:channel  subscription-channel
                          :matches? (make-filter filter)
                          :filter   filter}]
        (swap! subscriptions assoc uuid subscription)
        [uuid subscription-channel]))

    (-unsubscribe! [this uuid]
      (let [{:keys [channel]} (get @subscriptions uuid)]
        (swap! subscriptions dissoc uuid)
        (async/close! channel)
        true)))

(defn make [& [options]]
  (map->EventStore {:storage-path "data/events.store"}))
