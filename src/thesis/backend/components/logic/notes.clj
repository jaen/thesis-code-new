(ns thesis.backend.components.logic.notes
  (:require [com.stuartsierra.component :as component]
            [schema.core :as schema]
            [bouncer.core :as bouncer]
            [bouncer.validators :as v]
            [cats.monad.either :as me]
            [buddy.hashers :as hashers]

            [thesis.backend.components.protocols :as protocols]
            [cats.monad.maybe :as maybe]
            [taoensso.timbre :as log]
            [cats.core :as m]))

(def ^:private note-create-validator
  {:user  [v/required]
   :title [v/required [v/max-count 32]]
   :text  [v/required [v/max-count 256]]
   :mood  [v/required]})

(defn ^:private note-create-handler [_ {:as params} repository]
  (let [[errors? _] (bouncer/validate params note-create-validator)]
    (if-not errors?
      (let [existing (protocols/retrieve repository {:aggregate/fields {:user  (:user params)
                                                                        :title (:title params)}})]
        (if (maybe/nothing? existing)
          (me/right [:note/created {:payload       params
                                    :event-version 1}])
          (me/left {:title "note title must be unique"})))
      (me/left errors?))))

;;

(def ^:private note-update-validator
  {:id    [v/required]
   ;:user  []
   :title [[v/max-count 32]]
   :text  [[v/max-count 256]]
   ;:mood  []
   })

(defn ^:private note-update-handler [_ {:as params} repository]
  (let [[errors? _] (bouncer/validate params note-update-validator)]
    (if-not errors?
      (let [id       (:id params)
            existing (protocols/retrieve repository {:aggregate/id id})]
        (if (maybe/just? existing)
          (me/right [:note/updated {:aggregate-id  id
                                    :payload       (dissoc params :id)
                                    :event-version 1}])
          (me/left {:id "note does not exist"})))
      (me/left errors?))))

;;

(defn- note-created-handler [_ {:keys [:event/payload]} _] ; last is state, each hydration fold can have state
  ; which can be useful for things like averages, medians
  ; and such
  (maybe/just payload))

(defn- note-updated-handler [aggregate {:keys [:event/payload]} _] ; last is state, each hydration fold can have state
  ; which can be useful for things like averages, medians
  ; and such
  (maybe/just (merge aggregate payload)))

;;

(defn- note-query-view [_ view-id repository]
  (let [user (protocols/retrieve repository {:aggregate/type :note
                                             :aggregate/id view-id})]
    (m/<$> (fn [{:keys [aggregate/id aggregate/type aggregate/created-on
                        aggregate/updated-on aggregate/version aggregate/fields]}]
             {:aggregate/id         id
              :aggregate/created-on created-on
              :aggregate/updated-on updated-on
              :aggregate/version    version
              :aggregate/type       type
              :note/title           (:title fields)
              :note/text            (:text fields)
              :note/mood            (:mood fields)
              :note/user            (:user fields)})
      user)))

;;

(defrecord Notes []
  component/Lifecycle
    (start [component]
      component)

    (stop [component]
      component)

  protocols/CommandHandlerProvider
    (-get-command-handlers [this]
      {:note/create               note-create-handler
       :note/update               note-update-handler})

  protocols/DomainEventProvider
    (-get-event-handlers [this]
      {[:note :created 1] note-created-handler
       [:note :updated 1] note-updated-handler})

  protocols/ViewHandlerProvider
    (-get-view-handlers [this]
      {:notes/query {:filter  {:aggregate/type :note}
                     :view-fn note-query-view}}))

(defn make [& [options]]
  (map->Notes {}))
