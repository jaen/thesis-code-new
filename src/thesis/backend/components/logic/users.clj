(ns thesis.backend.components.logic.users
  (:require [com.stuartsierra.component :as component]
            [schema.core :as schema]
            [bouncer.core :as bouncer]
            [bouncer.validators :as v]
            [cats.monad.either :as me]
            [buddy.hashers :as hashers]

            [thesis.backend.components.protocols :as protocols]
            [cats.monad.maybe :as maybe]
            [taoensso.timbre :as log]
            [cats.core :as m]))

(def ^:private user-create-validator
  {:username [v/required [v/max-count 32]]
   :password [v/required [v/max-count 32]]})

(defn- hash-password [password]
  (hashers/encrypt password {:algorithm :scrypt :cpucost 65536 :memcost 8 :parallelism 1}))

(defn ^:private users-create-handler [_ {:as params} repository]
  (let [[errors? _] (bouncer/validate params user-create-validator)]
    (if-not errors?
      (let [existing (protocols/retrieve repository {:aggregate/fields {:username (:username params)}})]
        (if (maybe/nothing? existing)
          (me/right [:user/created {:payload       (update params :password hash-password)
                                    :event-version 1}])
          (me/left {:username "username already taken"})))
      (me/left errors?))))

(def ^:private user-update-personal-info-validator
  {:id       [v/required] ; TODO: validate type later
   :username [[v/max-count 32]]})

(defn ^:private users-update-personal-info-handler [_ {:as params} repository]
  (let [[errors? _] (bouncer/validate params user-update-personal-info-validator)]
    (if-not errors?
      (let [id       (:id params)
            existing (protocols/retrieve repository {:aggregate/id id})]
        (if (maybe/just? existing)
          (me/right [:user/updated-personal-info {:aggregate-id  id
                                                  :payload       (dissoc params :id)
                                                  :event-version 1}])
          (me/left {:id "user does not exist"})))
      (me/left errors?))))

;;

(defn- users-created-handler [_ {:keys [event/payload]} _] ; last is state, each hydration fold can have state
                                                            ; which can be useful for things like averages, medians
                                                            ; and such
  (maybe/just payload))

(defn- users-updated-personal-info-handler [aggregate {:keys [event/payload]} _]
  (maybe/just (merge aggregate payload)))

;;

(defn- users-query-view [_ view-id repository]
  (let [user (protocols/retrieve repository {:aggregate/type :user
                                             :aggregate/id view-id})]
    (m/<$> (fn [{:keys [aggregate/id aggregate/type aggregate/created-on
                        aggregate/updated-on aggregate/version aggregate/fields]}]
             (merge {:aggregate/id         id
                     :aggregate/created-on created-on
                     :aggregate/updated-on updated-on
                     :aggregate/version    version
                     :aggregate/type       type
                     :user/id id ; coz hav
                     :user/username (:username fields)
                     }
               #_(dissoc fields :password)))
      user)))

;;

(defrecord Users []
  component/Lifecycle
    (start [component]
      component)

    (stop [component]
      component)

  protocols/CommandHandlerProvider
    (-get-command-handlers [this]
      {:user/create               users-create-handler
       :user/update-personal-info users-update-personal-info-handler})

  protocols/DomainEventProvider
    (-get-event-handlers [this]
      {[:user :created 1]               users-created-handler
       [:user :updated-personal-info 1] users-updated-personal-info-handler})

  protocols/ViewHandlerProvider
    (-get-view-handlers [this]
      {:users/query {:filter  {:aggregate/type :user}
                     :view-fn users-query-view}}))

(defn make [& [options]]
  (map->Users {}))
