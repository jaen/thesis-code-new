(ns thesis.backend.components.protocols
  (:require [schema.core :as s]
            [thesis.backend.schemas :refer [Event]]))

(defprotocol DomainEventProvider
  (-get-event-handlers [this]))

(defprotocol AsyncHandlerProvider
  (-get-async-handlers [this]))

(defprotocol ViewHandlerProvider
  (-get-view-handlers [this]))

(defprotocol WebServiceProvider
  (-get-routes [this])
  (-get-handlers [this]))

(defprotocol Logic
  (-handle-command! [this command params])
  (-query-view [this query]))

(defprotocol CommandHandlerProvider
  (-get-command-handlers [this]))

(defprotocol CommandDispatcher
  (-run-command! [this command params]))

(defprotocol EventStore
  (-append-event!   [this event])
  (-retrieve-events [this filter]))

(defprotocol Subscribable
  (-subscribe!      [this filter])
  (-unsubscribe!    [this uuid]))

(defprotocol Repository
  (-retrieve [this filter]))

(defprotocol ViewStore
  (-retrieve-view [this view-name view-id])
  (-query-view'    [this view-name query]))

(defprotocol AggregateCache
  (-cache!    [this aggregate])
  (-retrieve! [this uuid])
  (-evict!    [this uuid]))

(defn retrieve! [cache uuid]
  (-retrieve! cache uuid))

(defn cache! [cache aggregate]
  (-cache! cache aggregate))

(defn evict! [cache uuid]
  (-evict! cache uuid))

;; wrapper functions

  (defn get-event-handlers [provider]
    (-get-event-handlers provider))

  (defn handle-command! [logic command params]
    (-handle-command! logic command params))

  (defn query-view [logic query]
    (-query-view logic query))

  (s/defn append-event!
    [store event :- Event]

    (-append-event! store event))

  (s/defn retrieve-events ; :- (Maybe Event)
    [store filter]

    (-retrieve-events store filter))

  (s/defn subscribe!
    [store filter]

    (-subscribe! store filter))

  (s/defn unsubscribe!
    [store uuid]

    (-unsubscribe! store uuid))

  (defn run-command! [dispatcher command params]
    (-run-command! dispatcher command params))

  (defn retrieve [repository filter]
    (-retrieve repository filter))

  (defn retrieve-view [view-store view-name params]
    (-retrieve-view view-store view-name params))

  (defn get-view-handlers [provider]
    (-get-view-handlers provider))

