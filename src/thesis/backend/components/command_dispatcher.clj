(ns thesis.backend.components.command-dispatcher
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [cats.core :as m]
            [taoensso.truss :as truss :refer (have have! have?)]
            [schema.core :as s]
            [clj-uuid]
            [clj-time.core :as time]


            [thesis.common.utils.core :refer :all]
            [thesis.backend.components.protocols :as protocols]
            [cats.monad.either :as either])
  (:import [clojure.lang MultiFn]))

(defn- make-dispatch-method []
  (new MultiFn "thesis.backend.components.command-dispatcher/command-dispatch-method"
    (fn [command _ _]
      command)
    :default
    #'clojure.core/global-hierarchy))

(defn- generate-event-id! [aggregate-type event-type version]
  (clj-uuid/squuid))

(defn- generate-aggregate-id! [aggregate-type]
  (clj-uuid/squuid))

(def ^:private EmitEventParams
  [(s/one s/Keyword "event-name")
   (s/one {(s/optional-key :aggregate-id) s/Uuid
           (s/optional-key :aggregate-version) s/Int
           :event-version s/Int
           :payload {s/Any s/Any}} "event-params")])

(s/defn ^:private emit-event! [event-store event-vec :- EmitEventParams]
  (let [[event params ]   event-vec
        aggregate-type    (keyword (namespace event))
        event-type        (keyword (name event))
        event-version     (:event-version params)
        aggregate-version (or (:aggregate-version params) 1)
        event-id          (generate-event-id! aggregate-type event-type event-version)
        aggregate-id      (or (:aggregate-id params) (generate-aggregate-id! aggregate-type))
        event             {:event/id event-id
                           :event/type event-type
                           :event/version event-version
                           :aggregate/version aggregate-version
                           :aggregate/id aggregate-id
                           :aggregate/type aggregate-type
                           :event/timestamp (time/now)
                           :event/payload (:payload params)}]
    (have! some? (or aggregate-id (= event-type :created))) ; only create can omit aggregate id
    (log/debug "Will append event: " event)
    (protocols/append-event! event-store event)))

(defrecord CommandDispatcher [event-store repository dispatch-method]
  component/Lifecycle
    (start [component]
      (log/debug "Starting command dispatcher")
      (let [dispatch-method (make-dispatch-method)
            command-handlers (filter #(satisfies? protocols/CommandHandlerProvider %)
                                     (vals component))]
        (doseq [[dispatch-value handler] (mapcat protocols/-get-command-handlers command-handlers)]
          (log/debug "Registering command handler for" dispatch-value)
          (.addMethod ^MultiFn dispatch-method dispatch-value handler))
        (.addMethod ^MultiFn dispatch-method :default (fn [command _ _]
                                                        (log/debug "Unknown command: " command)
                                                        (either/left {:unknown-command command})))
        (log/debug "Started command dispatcher")
        (assoc component :dispatch-method dispatch-method)))

    (stop [component]
      component)

  protocols/CommandDispatcher
    (-run-command! [this command params]
      (let [result (dispatch-method command params repository)]
        (log/debug "COMMAND RESULT " result)
        (m/>>= result
               (partial emit-event! event-store)))))

(defn make [& [options]]
  (map->CommandDispatcher {}))