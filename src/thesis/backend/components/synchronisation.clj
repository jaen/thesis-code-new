(ns thesis.backend.components.synchronisation
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [clojure.core.async :as async :refer [<! >! go go-loop]]

            [thesis.backend.components.protocols :as protocols]
            [cats.core :as m]
            [cats.monad.maybe :as maybe]
            [clojure.walk :as walk]))
#_{:event :synchronisation/query
   :client-id "9880934d-437a-49f7-9861-eac15aa9fc86"
   :user-id "9880934d-437a-49f7-9861-eac15aa9fc86"
   :message-id #uuid "131d3806-aad7-45e9-8b50-7636ee791b0b"
   :payload {:name query-name32247,
             :query (User [:username :id]), :args {}}
   :current-user nil}

(defn repository [] (:repository reloaded.repl/system))
(defn view-store [] (:view-store reloaded.repl/system))
(defn async [] (:async reloaded.repl/system))

(defn filter-from-params [namespace params]
  (into {} (->> params
                (filter map?)
                (map (fn [contraint-map]
                       (let [[field constraint] (first contraint-map)]
                         [(keyword namespace (name field)) constraint]))))))

(defn filter-fields [namespace entity params]
  (let [fields (conj
                 (map (fn [param]
                        (cond
                          (map? param)
                            (keyword namespace (name (first (keys param))))
                            #_(first (keys param))
                          :else
                            #_param
                            (keyword namespace (name param))))
                   params)
                 :aggregate/type
                 :aggregate/id)]
    ;(log/debug "ENTITY: " entity)
    ;(log/debug "NEEDS FIELDS: " fields)
    (select-keys entity fields)))

(defmulti fetch-entity (fn [entity params] entity))

(defmethod fetch-entity 'User [_ params]
  (let [repostitory (repository)
        view-store (view-store)
        merged-filter (merge
                        (filter-from-params "user" params)
                        {:aggregate/type :user})
         _ (log/debug "MERGED FILTER: " merged-filter)
        ; _ (println "MERGED FILTER: " merged-filter)
        query-result (protocols/-query-view'
                       view-store
                       :users/query
                       merged-filter)]
    (log/debug "OMG QUERY RESULT:" query-result)
    (m/<$> (fn [entities]
             (let [result (cond
                            (vector? entities) (mapv #(filter-fields "user" % params) entities)
                            :else              [(filter-fields "user" entities params)])]
               result #_(map )))
      query-result)))

(defmethod fetch-entity 'Note [_ params]
  #_(log/debug "WANTS TO FETCH" params)
  (let [view-store (view-store)
        merged-filter (merge
                        (filter-from-params "note" params)
                        {:aggregate/type :note})
        query-result (protocols/-query-view'
                       view-store
                       :notes/query
                       merged-filter)]
    ;(log/debug "PARAMS ARE " params)
    ;(log/debug "FILTER WOULD BE " merged-filter)
    ;(log/debug "OMG QUERY RESULT:" query-result)
    (m/<$> (fn [entities]
             (let [result (cond
                            (vector? entities) (mapv #(filter-fields "note" % params) entities)
                            :else              [(filter-fields "note" entities params)])]
               (mapv #(update % :note/user (fn [val] [:user/id val])) result)))
      query-result)
    #_(maybe/just [{:aggregate/id #uuid "ba0fc831-c919-11e5-af4b-753e477beb2a"
                  :note/id #uuid "ba0fc831-c919-11e5-af4b-753e477beb2a"
                  :aggregate/type :note
                  :note/title "Test title"
                  :note/text  "Test text"
                  :note/user [:user/id #uuid "ba0fc831-c919-11e5-af4b-753e477beb2f"]}])))

(defn- execute-query [query]
  (cond
    (seq? query) (let [[entity params] query]
                    (log/debug "KEEEEEK" entity params)
                    (fetch-entity entity params)
                    #_(maybe/just {:fuck "this"}))))

(defonce active-queries (atom {}))

; TODO: make less hardcoded
(defn analyse-query [query]
  (let [[entity params] query
        entity-view (get {'Note :notes/query 'User :users/query} entity)
        entity-type (get {'Note :note 'User :user} entity)
        filter-map (filter-from-params (name entity-type) params)]
    {:query query
     :view entity-view
     :type entity-type
     :filter filter-map}))

(defn watch-query! [user-id query]
  (let [view-store (view-store)
        async (async)
        send! (:send! async)]
    (let [[uuid subscription-chan] (protocols/subscribe! view-store query)
          watch-keys {:channel         subscription-chan
                      :subscription-id uuid}]
      (go
        (loop []
          (if-let [new-view (<! subscription-chan)]
            (let [new-view (if (= (:aggregate/type new-view) :note)
                             ;(select-keys
                             (update new-view :note/user (fn [val] [:user/id val]))
                             ;[:aggregate/id :aggregate/type :note/title :note/text :note/mood :note/user])
                             new-view)]
              (log/debug "!!! JUST RECEIVED NEW VIEW !!!" new-view)
              (send!
                user-id
                :synchronisation/update
                {:result [new-view]})
              (recur))
            (log/debug "QUERY UNWATCHED:" user-id query))))
      (merge query
        watch-keys))))

(defn- query [request]
  (let [{:keys [name query args]} (:payload request)
        async (async)
        view-store (view-store)
        args (into {} (map (fn [[k v]]
                             (cond
                               (vector? v)
                                 [k (get v 1)]
                               :else
                                 [k v]))
                        args))
        _ (log/debug "ARGS" args)
        substituted-query (walk/prewalk-replace args query)
        analysed-query (analyse-query substituted-query)
        user-id (:user-id request)
        watched-query (if-let [existing-query (get-in @active-queries [user-id name])]
                        (do
                          (log/debug "WILL RE-WATCH" user-id analysed-query)
                          (protocols/unsubscribe! view-store (:subscription-id existing-query)) ; end watch
                          (watch-query! user-id analysed-query))
                        (do
                          (log/debug "WILL WATCH" user-id analysed-query)
                          (watch-query! user-id analysed-query)))
        ]
    (log/debug "received synchronisation request: " (dissoc request :ev-msg))
    (swap! active-queries update-in [user-id] assoc name watched-query)
    (log/debug "CURRENTLY ACTIVE QUERIES: " @active-queries)
    (m/<$> (fn [result]
             (log/debug "would reply with: " result)
             ((:send! async)
              user-id
              :synchronisation/update
              {:result result}))
      (execute-query substituted-query))))

(defn unsubscribe [request]
  )

(defrecord Synchronisation [async view-store]
  component/Lifecycle
  (start [component]
    component)

  (stop [component]
    component)

  protocols/AsyncHandlerProvider
    (-get-async-handlers [this]
      {:synchronisation/query query
       :synchronisation/unsubscribe unsubscribe}))

(defn make [& [options]]
  (map->Synchronisation {}))
