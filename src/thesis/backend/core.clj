(ns thesis.backend.core
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [taoensso.timbre.appenders.core :as appenders]
            [schema.core :as s]
            [tangrammer.component.co-dependency :as co-dependency]

            [thesis.backend.config :as config]
            [thesis.backend.components.webserver :as webserver]
            [thesis.backend.components.application :as application]
            [thesis.backend.components.logic :as logic]
            [thesis.backend.components.async :as async]
            [thesis.backend.components.repository :as repository]
            [thesis.backend.components.command-dispatcher :as command-dispatcher]
            [thesis.backend.components.event-store :as event-store]
            [thesis.backend.components.logic.users :as users]
            [thesis.backend.components.view-store :as view-store]
            [thesis.backend.components.logic.notes :as notes]
            [thesis.backend.components.synchronisation :as synchronisation]
            [thesis.backend.components.aggregate-cache :as aggregate-cache])
  (:gen-class))

;;

  (s/set-fn-validation! true)

  (log/merge-config!
    {:appenders {:println {:enabled? false}
                 :spit    (appenders/spit-appender {:fname "./logs/timbre.log"})}})

;; System

  (defn make-system!
    "Returns a system representing the application."
    []

    (let [{:keys [env port ssl-port keystore-path
                  truststore-path] :as config}    (config/get)
          immutant-params {:host "0.0.0.0"
                           :port port
                           :ssl-port ssl-port
                           :http2? true
                           :keystore keystore-path
                           :key-password "panda5"
                           :truststore truststore-path
                           :trust-password "panda5"}]

      (log/info "Application config: " config)
      (component/system-map
        :aggregate-cache    (aggregate-cache/make)
        :synchronisation    (-> (synchronisation/make)
                                (component/using [:view-store])
                                (co-dependency/co-using
                                  [:async] ))
        :notes              (-> (notes/make)
                                (component/using []))
        :users              (-> (users/make)
                                (component/using []))
        :repository         (-> (repository/make)
                                (component/using [:users :notes :event-store :aggregate-cache]))
        :view-store         (-> (view-store/make)
                                (component/using [:event-store :repository :users :notes]))
        :event-store        (-> (event-store/make)
                                (component/using []))
        :command-dispatcher (-> (command-dispatcher/make)
                                (component/using [:event-store :repository :users :notes]))
        :logic              (-> (logic/make)
                                (component/using [:command-dispatcher]))
        :async              (-> (async/make)
                                (component/using [:synchronisation]))
        :application        (-> (application/make)
                                (component/using [:logic :async]))
        :web                (-> (webserver/make immutant-params)
                                (component/using [:application])))))

  (defn -main [& args]
    (let [system (make-system!)]
      (component/start system)))
