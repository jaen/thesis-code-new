(ns thesis.common.utils.transit
  (:refer-clojure :exclude [read])
  (:require [cognitect.transit :as transit]
            #?@(:cljs [[datascript.transit :as dt]])
            [thesis.common.utils.time :as time-utils])
  #?(:clj  (:import (org.joda.time DateTime)
                    (java.io ByteArrayInputStream ByteArrayOutputStream))
     :cljs (:import (goog.date Date DateTime UtcDateTime))))

(def ^:private clj-time-writer
  "Transit writer for clj-time objects."

  (transit/write-handler
    (constantly "m")
    (fn [v] (time-utils/to-nanos v))
    (fn [v] (str (time-utils/to-nanos v)))))

(def ^:private clj-time-reader
  "Transit reader for clj-time objects."

  (transit/read-handler
    (fn [v] (let [parsed-v v]
              (time-utils/from-nanos parsed-v)))))

(def transit-write-handlers
  "Transit write handlers for the application."

  #?(:clj  {DateTime clj-time-writer}
     :cljs (merge {goog.date.Date clj-time-writer
                   goog.date.DateTime clj-time-writer
                   goog.date.UtcDateTime clj-time-writer}
             dt/write-handlers)))

(def transit-read-handlers
  "Transit read handlers for the application."

  #?(:clj  {"m"                clj-time-reader}
     :cljs (merge {"m"                clj-time-reader}
             dt/read-handlers)))

#?(:clj
   (defmulti write
     "A mutlimethod to write out transit with a given format and using
      application readers and handlers."
     (fn [format _ _] format)))

#?(:clj
   (defmethod write :json [_ output-stream input]
     (let [transit-writer (transit/writer output-stream :json {:handlers transit-write-handlers})]
       (transit/write transit-writer input))))

#?(:clj
   (defmethod write :msgpack [_ output-stream input]
     (let [transit-writer (transit/writer output-stream :msgpack {:handlers transit-write-handlers})]
       (transit/write transit-writer input))))

#?(:clj
   (defmulti read (fn [format _] format)))

#?(:clj
   (defmethod read :json [_ output-stream]
     (let [transit-reader (transit/reader output-stream :json {:handlers transit-read-handlers})]
       (transit/read transit-reader))))

#?(:clj
   (defmethod read :msgpack [_ output-stream]
     (let [transit-reader (transit/reader output-stream :msgpack {:handlers transit-read-handlers})]
       (transit/read transit-reader))))

#?(:clj (defn to-str [obj]
          (let [string-writer  (ByteArrayOutputStream.)]
            (write :json string-writer obj)
            (.toString string-writer)))
   :cljs (defn to-str [obj]
           (let [writer (transit/writer :json {:handlers transit-write-handlers})]
             (transit/write writer obj))))

#?(:clj (defn from-str [str]
          (let [string-reader  (ByteArrayInputStream. (.getBytes str))]
            (read :json string-reader)))
   :cljs (defn from-str [str]
           (let [reader (transit/reader :json {:handlers transit-read-handlers})]
             (transit/read reader str))))



;(immutant-transit/register-transit-codec
; :name :transit-msgpack
; :type :msgpack
; :read-handlers read-handlers
; :write-handlers write-handlers)

;(immutant-transit/register-transit-codec
; :name :transit-json
; :type :json
; :read-handlers read-handlers
; :write-handlers write-handlers)