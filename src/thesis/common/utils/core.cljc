(ns thesis.common.utils.core)

;; Clojure, y u no zip : |
;(defn zip
;  "Zips collections."
;  [& colls]
;
;  (partition (count colls) (apply interleave colls)))

(defn zip
  "Zips collections."
  [& colls]

  (apply map vector colls))

(def concatv
  "Concat and return a vector."

  (comp vec concat))