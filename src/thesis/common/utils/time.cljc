(ns thesis.common.utils.time
  (:require #?@(:clj [[clj-time.format :as time-format]
                      [clj-time.coerce :as time-coerce]]
                :cljs [[cljs-time.format :as time-format]
                       [cljs-time.coerce :as time-coerce]])))

;; Generic formatter

  (def ^:private app-time-formatter
    "App-wide default date format."

    (time-format/formatters :date-time))

  (defn to-str
    "Return a string representation of `date-time` according to the `app-time-formatter`."
    [date-time]

    (time-format/unparse app-time-formatter date-time))

  (defn from-str
    "Parser `date-time-str` string according to the `app-time-formatter`."
    [date-time-str]

    (time-format/parse app-time-formatter date-time-str))

  (defn to-nanos
    "Returns time in nanoseconds since UNIX epoch."
    [date-time]

    (time-coerce/to-long date-time))

  (defn from-nanos
    "Creates a datetime from nanoseconds since UNIX epoch."
    [nanos]

    (time-coerce/from-long nanos))
