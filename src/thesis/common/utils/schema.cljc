(ns thesis.common.utils.schema
  (:require [schema-tools.core :as st]
            [schema-tools.walk :as st-walk]
            [clojure.string :as str]
            [schema.core :as s #?@(:cljs [:include-macros true])]
            [schema.spec.core :as spec]
            [schema.spec.variant :as variant]))

(defn with-optional-keys
  "Returns a schmea with keys recursively made optional."
  [schema]

  (st-walk/walk schema
    (fn [x]
      (let [y (with-optional-keys x)]
        (if (and (map? y) (not (record? y)))
          (st/optional-keys y)
          y)))
    identity))

;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Private: helpers

(defprotocol PExtensibleSchema
  (extend-schema! [this extension schema-name dispatch-values]))

;; a "subclass"
(defrecord SchemaExtension [schema-name base-schema extended-schema explain-value]
  s/Schema
  (spec [this]
    (variant/variant-spec spec/+no-precondition+ [{:schema extended-schema}]))
  (explain [this]
    (list 'extend-schema
      schema-name
      (s/schema-name base-schema)
      (s/explain explain-value))))

;; an "abstract class"
(defrecord AbstractSchema [sub-schemas dispatch-fn schema open?]
  s/Schema
  (spec [this]
    (variant/variant-spec
      spec/+no-precondition+
      (concat
        (do ;(println sub-schemas)
          (for [[k s] @sub-schemas]
              {:guard  (fn [val]
                         (let [res (= (dispatch-fn val) k)]
                            ;(println "VAL" val)
                            ;(println "FN" dispatch-fn)
                            ;(println "RES1 " (dispatch-fn val))
                            ;(println "RES2 " k)
                            ;(println "RES3 " res)
                           res))
               :schema s}))
        #_(when open?
          [{:schema (assoc schema dispatch-key s/Keyword s/Any s/Any)}]))
      (fn [v] (list (set (keys @sub-schemas)) (list dispatch-fn v)))))
  (explain [this]
    (list 'abstract-map-schema dispatch-fn (s/explain schema) (set (keys @sub-schemas))))

  PExtensibleSchema
  (extend-schema! [this extension schema-name dispatch-values]
    (let [sub-schema (merge schema extension)
          #_(assoc (merge schema extension)
                       dispatch-key (apply s/enum dispatch-values))
          ext-schema (s/schema-with-name
                       (SchemaExtension. schema-name this sub-schema extension)
                       (name schema-name))]
      (swap! sub-schemas merge (into {} (for [k dispatch-values] [k ext-schema])))
      ext-schema)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Public

(s/defn abstract-map-schema
  "A schema representing an 'abstract class' map that must match at least one concrete
   subtype (indicated by the value of dispatch-key, a keyword).  Add subtypes by calling
   `extend-schema`."
  [dispatch-fn :- (s/either s/Keyword (s/=> s/Any s/Any))
   schema :- (s/pred map?)]

  (AbstractSchema. (atom {}) dispatch-fn schema false))

(s/defn open-abstract-map-schema
  "Like abstract-map-schema, but allows unknown types to validate (for, e.g. forward
   compatibility)."
  [dispatch-fn :- (s/either s/Keyword (s/=> s/Any s/Any))
   schema :- (s/pred map?)]

  (AbstractSchema. (atom {}) dispatch-fn schema true))

(defmacro extend-schema
  [schema-name extensible-schema dispatch-values extension]
  `(def ~schema-name
     (extend-schema! ~extensible-schema ~extension '~schema-name ~dispatch-values)))

(defn sub-schemas [abstract-schema]
  @(.-sub-schemas ^AbstractSchema abstract-schema))