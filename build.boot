#!/usr/bin/env boot

(set-env!
 :source-paths #{"src" "dev"}
 :resource-paths #{"resources"})

(require 'build-utils)

(set-env!
 :dependencies (build-utils/read-dependencies!)
 :exclusions '[org.clojure/clojure org.clojure/clojurescript]
 :main-class 'thesis.core)

(task-options! aot {:namespace #{(get-env :main-class)}}
               jar {:main (get-env :main-class)}
               pom {:project 'thesis
                    :version "1.0.0"
                    :url "https://gitlab.com/jaen/thesis"})

(require '[pandeiro.boot-http :as bh]
         '[adzerk.boot-reload :as br]
         '[adzerk.boot-cljs :as bc]
         '[adzerk.boot-cljs-repl :as bcr]
         '[boot-notify :as bn]
         '[boot.immutant :as immutant]
         '[boot.util :as util]
         '[boot.pod :as pod]

         '[com.joshuadavey.boot-middleman :as bm]
         '[dev-handler]
         '[system.boot :as sb]
         )

(deftask dev
  "Starts the development environment."
  []

  ; (set-env! :source-paths #(conj % "dev"))
  (require 'thesis.backend.core)
  (let [system-var (resolve 'thesis.backend.core/make-system!)]
    (comp
      (watch)
      (build-utils/update-deps)
      (build-utils/lein-generate)
      (bh/serve :handler 'dev-handler/my-dir-handler :port 8080)
      (bm/middleman :out "public")
      (br/reload :on-jsload 'thesis.frontend.core/app-reload!)
      (bcr/cljs-repl)
      (bc/cljs :source-map true
               :optimizations :none
               :ids #{"public/assets/javascripts/application"}
               :compiler-options {;:parallel-build true
                                  :compiler-stats true})
      (bn/notify)
      #_(sb/system :sys system-var
                 :auto true)
      (sb/system :sys system-var
                 :auto-start true
                 :hot-reload true
                 :regexes #{#"^thesis/backend/" #"^thesis/common/"})
      (repl :server true
            :middleware '[dirac.nrepl.middleware/dirac-repl]
            :port 8230)
      (target :dir #{"target"}))))

(deftask build
  "Builds an uberjar of this project that can be run with java -jar"
  []

  (comp
   (aot)
   (pom)
   (uber)
   (jar)
   (target :dir #{"target"})))

(deftask build-immutant
  "Build a WidFly-compatible war."
  []

  (set-env! :resource-paths #{"src"})
  (comp
    (immutant/immutant-war :context-path "/"
                           :init-fn 'panda-5.core/start!
                           :name "ROOT"
                           :nrepl-host "0.0.0.0"
                           :nrepl-port 12132
                           :nrepl-start true)
    (target :dir #{"target"})))

(defn cljs-repl! []
  (bcr/start-repl))

(defn start-dirac! []
  (require 'dirac.agent)
  (let [dirac-boot! (resolve 'dirac.agent/boot!)]
    (dirac-boot!)))

(defn get-system []
  reloaded.repl/system)